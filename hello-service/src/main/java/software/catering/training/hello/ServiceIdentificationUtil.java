/*
 * ----------------------------------------------------------------------------
 * Copyright 2009 - 2017 by PostFinance AG - all rights reserved
 * ----------------------------------------------------------------------------
 */
package software.catering.training.hello;

import java.net.InetAddress;
import java.net.UnknownHostException;

final class ServiceIdentificationUtil {

	static String getServiceIdentification() {
		String result;

		try {
			result = InetAddress.getLocalHost().getHostAddress();
		} catch (UnknownHostException e) {
			e.printStackTrace();
			result = e.getMessage();
		}
		return result;
	}
}
