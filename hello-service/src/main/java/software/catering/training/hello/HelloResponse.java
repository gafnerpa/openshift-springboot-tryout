/*
 * ----------------------------------------------------------------------------
 * Copyright 2009 - 2017 by PostFinance AG - all rights reserved
 * ----------------------------------------------------------------------------
 */
package software.catering.training.hello;

import java.util.Date;

public class HelloResponse {
	private String message;
	public String senderIp = ServiceIdentificationUtil.getServiceIdentification();
	public String timeStamp = new Date().toString();

	HelloResponse() {
	}

	HelloResponse(String message) {
		this.message = message;
	}
}
