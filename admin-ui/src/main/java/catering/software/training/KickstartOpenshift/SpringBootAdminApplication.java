/*
 * ----------------------------------------------------------------------------
 * Copyright 2009 - 2017 by PostFinance AG - all rights reserved
 * ----------------------------------------------------------------------------
 */
package catering.software.training.KickstartOpenshift;

import de.codecentric.boot.admin.config.EnableAdminServer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableAutoConfiguration
@EnableAdminServer
public class SpringBootAdminApplication {
	public static void main(String[] args) {
		SpringApplication.run(SpringBootAdminApplication.class, args);
	}
}
