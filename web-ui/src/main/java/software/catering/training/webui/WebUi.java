/*
 * ----------------------------------------------------------------------------
 * Copyright 2009 - 2017 by PostFinance AG - all rights reserved
 * ----------------------------------------------------------------------------
 */
package software.catering.training.webui;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebUi {

	public static void main(String[] args) {
		SpringApplication.run(WebUi.class, args);
	}
}
